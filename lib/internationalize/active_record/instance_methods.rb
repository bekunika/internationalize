module Internationalize
  module ActiveRecord
    module InstanceMethods
      def id=(value)
        if id
          translations.each do |translation|
            translation.preparation_id = value # TODO fix? get field name from model name
          end
        end
        super
      end

      def validate_having_translations
        errors.add(:base, :without_translates) if translations.empty?
      end

      def locale(locale)
        translation = translations.select { |tr| tr.locale.to_s == locale.to_s }.first
        translation || translations.build(locale: locale)
      end

      def translation
        locale I18n.locale
      end

      def type_for_attribute(attr_name, &block)
        attr_name = attr_name.to_s
        if block
          super
        else
          super_call = super
          super_call.type
          if super_call.type
            super_call
          else
            translation.class.type_for_attribute(attr_name, &block)
          end
        end
      end
    end
  end
end
