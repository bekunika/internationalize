module Internationalize
  module ActiveRecord
    class Translation < ::ApplicationRecord
      self.abstract_class = true
    end
  end
end
